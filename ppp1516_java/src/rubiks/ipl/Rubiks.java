package rubiks.ipl;

import ibis.ipl.*;
import java.io.*;

/**
 * Solver for rubik's cube puzzle, adapted to run in parallel using IPL.
 * 
 * @author Niels Drost, Timo van Kessel, adaptation by Dylan Goldsborough
 * 
 */
public class Rubiks {

    static IbisCapabilities ibisCapabilities = new IbisCapabilities(IbisCapabilities.ELECTIONS_STRICT,IbisCapabilities.CLOSED_WORLD);
    
    static PortType portType = new PortType(PortType.COMMUNICATION_RELIABLE,PortType.SERIALIZATION_OBJECT_SUN, 
        PortType.RECEIVE_AUTO_UPCALLS, PortType.CONNECTION_DOWNCALLS,
        PortType.RECEIVE_EXPLICIT, PortType.CONNECTION_MANY_TO_MANY);
    
    public static final boolean PRINT_SOLUTION = false;
    private static int outstandingJobs;
    private static boolean printTime = false;
    private static int minTwistPar = 2; // performance parameter
    private static int startIteration = 1; // performance parameter
    private static int untilHereSeq = 4; // use sequential until here

    /**
     * Recursive function to find a solution for a given cube. Only searches to
     * the bound set in the cube object.
     * 
     * @param cube
     *            cube to solve
     * @param cache
     *            cache of cubes used for new cube objects
     * @return the number of solutions found
     */
    private static int solutions(Cube cube, CubeCache cache) {
        if (cube.isSolved()) {
            return 1;
        }

        if (cube.getTwists() >= cube.getBound()) {
            return 0;
        }

        // generate all possible cubes from this one by twisting it in
        // every possible way. Gets new objects from the cache
        Cube[] children = cube.generateChildren(cache);

        int result = 0;

        for (Cube child : children) {
            // recursion step
            int childSolutions = solutions(child, cache);
            if (childSolutions > 0) {
                result += childSolutions;
                if (PRINT_SOLUTION) {
                    child.print(System.err);
                    System.out.println(child.getTwists());
                }
            }
            // put child object in cache
            cache.put(child);
        }

        return result;
    }

    /**
     * Solves a Rubik's cube by iteratively searching for solutions with a
     * greater depth. This guarantees the optimal solution is found. Repeats all
     * work for the previous iteration each iteration though...
     * 
     * @param cube
     *            the cube to solve
     */
    private static void solve(Cube cube, int twists, int seed) throws IOException {
        Ibis ibis = null;
        try{
            ibis = IbisFactory.createIbis(ibisCapabilities, null, portType);
        } 
        catch(IbisCreationFailedException e) {
            System.out.println("Failed to create an Ibis.");
            System.exit(1);
        }

        // System.err.println(ibis.identifier());

        IbisIdentifier server = ibis.registry().elect("Server");
        ibis.registry().waitUntilPoolClosed();
        
        if (server.equals(ibis.identifier())) { 
            // print cube info
            System.out.println("Searching for solution for cube of size "
                    + cube.getSize() + ", twists = " + twists + ", seed = " + seed);
            cube.print(System.out);
            System.out.flush();
            solveServer(ibis, cube);
        } 
        else {
            solveClient(ibis, server);
        }

        ibis.end();
    }

    /**
     * Instructions for master nodes, it essentially fills the purpose of the
     * sequential solve method. However, instead of calling solutions itself,
     * it generates children of the inital cube, adds them to its cache, and
     * hands them out. 
     * 
     * @param myIbis
     *            its own ibis instant
     * @param cube
     *            the cube to solve
     */
    private static void solveServer(Ibis myIbis, Cube cube) throws IOException {
        printTime = true;
        // Create receive ports
        ReceivePort resultReceiver = myIbis.createReceivePort(portType,"ResultReceiver");
        ReceivePort phoneReceiver = myIbis.createReceivePort(portType,"PhoneReceiver");
        // Do a roll call using the resultReceiver
        // IbisIdentifier[] joinedIbises = myIbis.registry().joinedIbises();

        // Let's make a phonebook later, since joinedIbises is failing on me inexplicably and the rest works
        int poolSize = myIbis.registry().getPoolSize();
        poolSize = poolSize - 1; // Don't wait for yourself
        IbisIdentifier[] joinedIbises = new IbisIdentifier [poolSize];

        for(int i = 0; i < poolSize; i++){
            phoneReceiver.enableConnections();
            ReadMessage r = phoneReceiver.receive();
            int mes = r.readInt();
            IbisIdentifier targetClient = r.origin().ibisIdentifier();
            r.finish();
            phoneReceiver.disableConnections();
            joinedIbises[i] = targetClient;
            // System.out.println(targetClient);
        }

        int bound = 0;
        int result = 0;

        System.out.print("Bound now:");

        while (result == 0) {
            // Cache used as a workqueue for cube objects
            // Important, to not have any leftovers from previous bounds!
            CubeCache cache = new CubeCache(cube.getSize());
            bound++;
            cube.setBound(bound);

            System.out.print(" " + bound);
            // System.out.println("MUST BE ZERO: "+cache.getLength());
            if(bound < (untilHereSeq+1)) {
                result = solutions(cube, cache);
                if((result > 0)&&(poolSize != 0)) {
                    for(int i = 0; i < poolSize; i++){
                        try {
                            // Receive the nullmessage they want to send
                            resultReceiver.enableConnections();
                            ReadMessage r = resultReceiver.receive();
                            IbisIdentifier targetClient = r.origin().ibisIdentifier();
                            r.finish();
                            resultReceiver.disableConnections();

                            // Tell the node we are all done :(
                            // System.out.println(targetClient);
                            SendPort cubeSender = myIbis.createSendPort(portType);
                            cubeSender.connect(targetClient,"CubeGetter");
                            WriteMessage w = cubeSender.newMessage();
                            w.writeObject(null);
                            w.finish();
                            cubeSender.close();
                        } catch (Exception e) {
                            // ignored
                        }
                    }
                    broadcastStatus(myIbis, joinedIbises, false);
                }
            }
            else if(poolSize == 0) result = solutions(cube, cache);
            else result = outsource(myIbis, cube, resultReceiver, cache, bound, poolSize);

            // Broadcast whether we continue or not
            if((bound > untilHereSeq)&&(poolSize!=0)){
                if(result == 0) broadcastStatus(myIbis, joinedIbises, true);
                else broadcastStatus(myIbis, joinedIbises, false);
            }
        }

        System.out.println();
        System.out.println("Solving cube possible in " + result + " ways of "
                + bound + " steps");

        // Close receive port.
        resultReceiver.close();
        phoneReceiver.close();
    }

    private static void broadcastStatus(Ibis myIbis, IbisIdentifier [] joinedIbises, boolean status) throws IOException {
        for (int i = 0; i < joinedIbises.length; i++) {
            SendPort sendPort = myIbis.createSendPort(portType);
            // System.out.println(joinedIbises[i]);
            sendPort.connect(joinedIbises[i], "ReceiveContinueStatus");
            WriteMessage message = sendPort.newMessage();
            message.writeBoolean(status);
            message.finish();
            // System.out.println("Broadcast Success!");
            sendPort.close();
        }
        // System.out.println("Broadcast done, returning...");
    }

    /**
     * Instructions for slave nodes.
     * 
     * @param myIbis
     *            its own ibis instant
     * @param server
     *            the identifier for the master
     */
    private static void solveClient(Ibis myIbis, IbisIdentifier server) throws IOException {
        // Do a roll call
        SendPort phoneBookMaker = myIbis.createSendPort(portType);
        ReceivePort statusReceiver = myIbis.createReceivePort(portType, "ReceiveContinueStatus");
        // System.out.println(myIbis);
        phoneBookMaker.connect(server,"PhoneReceiver");
        WriteMessage w1 = phoneBookMaker.newMessage();
        w1.writeInt(1);
        w1.finish();

        boolean cont = true;
        while(cont==true){
            // System.out.println("round");
            inSource(myIbis, server);

            // Now we receive a broadcast
            statusReceiver.enableConnections();
            ReadMessage r = statusReceiver.receive();
            cont = r.readBoolean();
            r.finish();
            statusReceiver.disableConnections();
        }
        phoneBookMaker.close();
        statusReceiver.close();
    }

    private static int breakJobUp(Cube job, CubeCache workqueue){
        int solution = 0;
        // Check if it is solved already
        if (job.isSolved()) {
            solution = 1;
        }        
        // If not, put the children in the queue
        else {
            CubeCache temp = new CubeCache(job.getSize());
            Cube[] children = job.generateChildren(temp); // if you add workqueue it gets cleared!
            for (int i = 0; i < (children[0].getSize() - 1)*6; i++) {
                workqueue.put(children[i]);
            }
        }
        return solution;
    }

    private static int outsource(Ibis myIbis, Cube cube, ReceivePort resultReceiver, CubeCache cache, int bound, int slaveCount) throws IOException {
        // Only hand out jobs with 4 twists or more, for 3x3x3 cubes this is quite a large number
        // Avoid making many jobs of single cubes
        int minTwists = Math.min(bound,minTwistPar);

        // Reset the upcall-related variables
        outstandingJobs = 0;
        int collectedResults = 0;
        int iteration = startIteration; // tracks the number of jobs handled

        // Do things until the cache is empty, then return to solveServer
        cache.put(cube);
        CubeCache masterJobs = new CubeCache(cube.getSize());
        while(cache.getLength()!=0){
            // Get a cube to hand out from the queue
            Cube outCube = cache.get();
            if (outCube.getTwists()<minTwists){
                collectedResults = collectedResults + breakJobUp(outCube, cache);
            }

            // If it was twisted often enough, ship it to a slave!
            else {
                // Receive a result
                resultReceiver.enableConnections();
                ReadMessage r = resultReceiver.receive();
                int mes = r.readInt();
                IbisIdentifier targetClient = r.origin().ibisIdentifier();

                if(mes > -1){
                    collectedResults = collectedResults + mes;
                    reduceJob();
                }
                else{
                    System.err.println("The upcall was not a positive or zero integer!");
                }
                r.finish();
                resultReceiver.disableConnections();

                // Send the cube out to his ID!
                outstandingJobs++;
                SendPort cubeSender = myIbis.createSendPort(portType);
                cubeSender.connect(targetClient,"CubeGetter");
                WriteMessage w = cubeSender.newMessage();
                w.writeObject(outCube);
                w.finish();
                cubeSender.close();
                iteration++;
            }

            // Evaluate a job yourself after handing out jobs to slaves
           if((cache.getLength()!=0)&&(iteration%(slaveCount+1)==0)){
                Cube ownCube = cache.get();
                CubeCache solCache = new CubeCache(ownCube.getSize());
                collectedResults = collectedResults + solutions(ownCube,solCache);
            }
        }

        // Make sure there are no outstanding jobs, so wait for the last upcalls, every slave needs a null!
        for(int i = 0; i < slaveCount; i++){
            try {
                resultReceiver.enableConnections();
                ReadMessage r = resultReceiver.receive();
                int mes = r.readInt();
                IbisIdentifier targetClient = r.origin().ibisIdentifier();
                if(mes > -1){
                    collectedResults = collectedResults + mes;
                    reduceJob();
                }
                else{
                    System.err.println("The upcall was not a positive or zero integer!");
                }
                r.finish();
                resultReceiver.disableConnections();

                // Tell the node we are all done :(
                SendPort cubeSender = myIbis.createSendPort(portType);
                cubeSender.connect(targetClient,"CubeGetter");
                WriteMessage w = cubeSender.newMessage();
                w.writeObject(null);
                w.finish();
                cubeSender.close();
            } catch (Exception e) {
                // ignored
            }
        }

        return collectedResults;
    }

    /**
     * Instructions for slave nodes, it receives jobs from the master and
     * evaluates them using the solve method. It returns the number of 
     * solutions in this part of the tree to the master.
     * 
     * @param myIbis
     *            its own ibis instant
     * @param server
     *            the identifier for the master
     */
    private static void inSource(Ibis myIbis, IbisIdentifier server) throws IOException {
        ReceivePort receiveCube = myIbis.createReceivePort(portType,"CubeGetter");
        int result = 0;
        Cube inCube = null;
        do{
            // Get a cube from the server, with a number of twists done and a bound set
            // Simultaneously sent results from the previous run, initially this is zero
            SendPort requestCube = myIbis.createSendPort(portType);
            requestCube.connect(server,"ResultReceiver");
            WriteMessage w1 = requestCube.newMessage();
            w1.writeInt(result);
            w1.finish();
            requestCube.close();
            
            // Get a cube
            receiveCube.enableConnections();
            ReadMessage r1 = receiveCube.receive();
            inCube = null;
            try {
                inCube = (Cube) r1.readObject();
            }
            catch(ClassNotFoundException e) {
                System.err.println("Error: you didn't pass a cube!");
                System.exit(1);
            }
            r1.finish();
            receiveCube.disableConnections();

            // Feed it into the solutions method
            if(inCube != null) {
                CubeCache cache = new CubeCache(inCube.getSize());
                result = solutions(inCube, cache);
            }
        }while(inCube != null);
        receiveCube.close();
        // System.out.println(myIbis);
    }

    private static void reduceJob(){
        if(outstandingJobs!=0) outstandingJobs = outstandingJobs - 1;
    }

    public static void printUsage() {
        System.out.println("Rubiks Cube solver");
        System.out.println("");
        System.out
                .println("Does a number of random twists, then solves the rubiks cube with a simple");
        System.out
                .println(" brute-force approach. Can also take a file as input");
        System.out.println("");
        System.out.println("USAGE: Rubiks [OPTIONS]");
        System.out.println("");
        System.out.println("Options:");
        System.out.println("--size SIZE\t\tSize of cube (default: 3)");
        System.out
                .println("--twists TWISTS\t\tNumber of random twists (default: 11)");
        System.out
                .println("--seed SEED\t\tSeed of random generator (default: 0");
        System.out
                .println("--threads THREADS\t\tNumber of threads to use (default: 1, other values not supported by sequential version)");
        System.out.println("");
        System.out
                .println("--file FILE_NAME\t\tLoad cube from given file instead of generating it");
        System.out.println("");
    }

    /**
     * Main function.
     * 
     * @param arguments
     *            list of arguments
     */
    public static void main(String[] arguments) throws IOException {
        Cube cube = null;

        // default parameters of puzzle
        int size = 3;
        int twists = 11;
        int seed = 0;
        String fileName = null;

        // number of threads used to solve puzzle
        // (not used in sequential version)

        for (int i = 0; i < arguments.length; i++) {
            if (arguments[i].equalsIgnoreCase("--size")) {
                i++;
                size = Integer.parseInt(arguments[i]);
            } else if (arguments[i].equalsIgnoreCase("--twists")) {
                i++;
                twists = Integer.parseInt(arguments[i]);
            } else if (arguments[i].equalsIgnoreCase("--seed")) {
                i++;
                seed = Integer.parseInt(arguments[i]);
            } else if (arguments[i].equalsIgnoreCase("--file")) {
                i++;
                fileName = arguments[i];
            } else if (arguments[i].equalsIgnoreCase("--help") || arguments[i].equalsIgnoreCase("-h")) {
                printUsage();
                System.exit(0);
            } else {
                System.err.println("unknown option : " + arguments[i]);
                printUsage();
                System.exit(1);
            }
        }

        // create cube
        if (fileName == null) {
            cube = new Cube(size, twists, seed);
        } else {
            try {
                cube = new Cube(fileName);
            } catch (Exception e) {
                System.err.println("Cannot load cube from file: " + e);
                System.exit(1);
            }
        }

        // solve
        long start = System.currentTimeMillis();
        solve(cube, twists, seed);
        long end = System.currentTimeMillis();

        // NOTE: this is printed to standard error! The rest of the output is
        // constant for each set of parameters. Printing this to standard error
        // makes the output of standard out comparable with "diff"
        if (printTime==true) System.err.println("Solving cube took " + (end - start)
                + " milliseconds");
    }

}
