
#include <CImg.h>
#include <iostream>
#include <iomanip>
#include <string>

using cimg_library::CImg;
using std::cout;
using std::cerr;
using std::endl;
using std::fixed;
using std::setprecision;
using std::string;


extern int triangularSmooth(const int width, const int height, const int spectrum, unsigned char * inputImage, unsigned char * smoothImage);

void convertToInterleaved(int w, int h, unsigned char* untangled, unsigned char* tangled) {
    int numPixels = w*h;
    int numColors = 3;
    for(int i=0; i<numPixels; ++i) {
        int indexIntoInterleavedTuple = numColors*i;
        //Red
        tangled[indexIntoInterleavedTuple] = untangled[i];
        //Green
        tangled[indexIntoInterleavedTuple+1] = untangled[numPixels+i];
        //Blue
        tangled[indexIntoInterleavedTuple+2] = untangled[2*numPixels+i];
    }
}

void convertFromInterleaved(int w, int h, unsigned char* tangled, unsigned char* untangled) {
    int numPixels = w*h;
    int numColors = 3;
    for(int i=0; i<numPixels; ++i) {
        int indexIntoInterleavedTuple = numColors*i;
        //Red
        untangled[i] = tangled[indexIntoInterleavedTuple];
        //Green
        untangled[numPixels+i] = tangled[indexIntoInterleavedTuple+1];
        //Blue
        untangled[2*numPixels+i] = tangled[indexIntoInterleavedTuple+2];
    }
}

int main(int argc, char *argv[]) {

	if ( argc != 2 ) {
		cerr << "Usage: " << argv[0] << " <filename>" << endl;
		return 1;
	}

	// Load the input image
	CImg< unsigned char > inputImage = CImg< unsigned char >(argv[1]);
	if ( inputImage.spectrum() != 3 ) {
		cerr << "The input must be a color image." << endl;
		return 1;
	}
	int width = inputImage.width();
	int height = inputImage.height();

	unsigned char *untangled = inputImage.data();
	CImg< unsigned char > inputImageInterleaved = CImg< unsigned char >(width, height, 1, inputImage.spectrum());
	unsigned char *tangled = inputImageInterleaved.data();
	convertToInterleaved(width, height, untangled, tangled);

	// Apply the triangular smooth
	CImg< unsigned char > smoothImage = CImg< unsigned char >(inputImage.width(), inputImage.height(), 1, inputImage.spectrum());
	CImg< unsigned char > smoothImageUninterleaved = CImg< unsigned char >(inputImage.width(), inputImage.height(), 1, inputImage.spectrum());

	triangularSmooth(width, height, inputImage.spectrum(), inputImageInterleaved.data(), smoothImage.data());

	unsigned char *untangledOut = smoothImageUninterleaved.data();
	unsigned char *tangledOut = smoothImage.data();
	convertFromInterleaved(width, height, tangledOut, untangledOut);

	// Save output
	smoothImageUninterleaved.save(("./" + string(argv[1]) + ".smooth.par.bmp").c_str());

	return 0;
}
