
#include <CImg.h>
#include <iostream>
#include <iomanip>
#include <string>

using cimg_library::CImg;
using std::cout;
using std::cerr;
using std::endl;
using std::fixed;
using std::setprecision;
using std::string;

extern int darkGray(const int width, const int height, const unsigned char * inputImage, unsigned char * darkGrayImage);

void convertToInterleaved(int w, int h, unsigned char* untangled, unsigned char* tangled) {
    int numPixels = w*h;
    int numColors = 3;
    for(int i=0; i<numPixels; ++i) {
        int indexIntoInterleavedTuple = numColors*i;
        //Red
        tangled[indexIntoInterleavedTuple] = untangled[i];
        //Green
        tangled[indexIntoInterleavedTuple+1] = untangled[numPixels+i];
        //Blue
        tangled[indexIntoInterleavedTuple+2] = untangled[2*numPixels+i];
    }
}

int main(int argc, char *argv[]) {
	if ( argc != 2 ) {
		cerr << "Usage: " << argv[0] << " <filename>" << endl;
		return 1;
	}
	
	// Load the input image
	CImg< unsigned char > inputImage = CImg< unsigned char >(argv[1]);
	// Interleaved format for the cuda stream, and to combine reads more easily

	if ( inputImage.spectrum() != 3 ) {
		cerr << "The input must be a color image." << endl;
		return 1;
	}
	int width = inputImage.width();
	int height = inputImage.height();

	unsigned char *untangled = inputImage.data();
	CImg< unsigned char > inputImageInterleaved = CImg< unsigned char >(width, height, 1, inputImage.spectrum());
	unsigned char *tangled = inputImageInterleaved.data();
	convertToInterleaved(width, height, untangled, tangled);
	
	// Create an output image
	CImg< unsigned char > darkGrayImage = CImg< unsigned char >(width, height, 1, 1);

	int status = darkGray(width, height, inputImageInterleaved.data(), darkGrayImage.data());

	// Save output
	darkGrayImage.save(("./" + string(argv[1]) + ".dark.par.bmp").c_str());

	return status;
}