#include <Timer.hpp>
#include <iostream>
#include <iomanip>
#include <cuda.h>

using LOFAR::NSTimer;
using std::cout;
using std::cerr;
using std::endl;
using std::fixed;
using std::setprecision;

#define THREADS_PER_BLOCK 256

__global__ void darkGrayKernel(const int N, unsigned char * inputImage, unsigned char * outputImage, const int offset) {
	// Kernel
	int RGBitem = offset + 4 * 3 * (threadIdx.x + blockIdx.x * blockDim.x);

	if(RGBitem < (3*N)){
		// Load 4 pixels using vectorized loads
		float grayPix = 0.0;
		uchar4 rgb[4];
		uint3 *uint3_text = reinterpret_cast<uint3 *>(&inputImage[RGBitem]);
    	uint3 uint3_var;

        // Vectorized load, disguise the unsigned chars as a uint3
        uint3_var = uint3_text[0];

    	// Recast data to uchar4
    	rgb[0] = *reinterpret_cast<uchar4 *>(&uint3_var.x);
    	rgb[1] = *reinterpret_cast<uchar4 *>(&uint3_var.y);
    	rgb[2] = *reinterpret_cast<uchar4 *>(&uint3_var.z);

    	// p1
		grayPix = ((0.3f * (static_cast< float >(rgb[0].x))) + (0.59f * (static_cast< float >(rgb[0].y))) + (0.11f * (static_cast< float >(rgb[0].z))));
		grayPix = (grayPix * 0.6f) + 0.5f;
		outputImage[RGBitem/3] = static_cast< unsigned char >(grayPix);
    	// p2
    	if((RGBitem+1) < (3*N)){
			grayPix = ((0.3f * (static_cast< float >(rgb[0].w))) + (0.59f * (static_cast< float >(rgb[1].x))) + (0.11f * (static_cast< float >(rgb[1].y))));
			grayPix = (grayPix * 0.6f) + 0.5f;
			outputImage[RGBitem/3 + 1] = static_cast< unsigned char >(grayPix);
		}
    	// p3
    	if((RGBitem+2) < (3*N)){
			grayPix = ((0.3f * (static_cast< float >(rgb[1].z))) + (0.59f * (static_cast< float >(rgb[1].w))) + (0.11f * (static_cast< float >(rgb[2].x))));
			grayPix = (grayPix * 0.6f) + 0.5f;
			outputImage[RGBitem/3 + 2] = static_cast< unsigned char >(grayPix);
		}
    	// p4
		if((RGBitem+3) < (3*N)){
			grayPix = ((0.3f * (static_cast< float >(rgb[2].y))) + (0.59f * (static_cast< float >(rgb[2].z))) + (0.11f * (static_cast< float >(rgb[2].w))));
			grayPix = (grayPix * 0.6f) + 0.5f;
			outputImage[RGBitem/3  + 3] = static_cast< unsigned char >(grayPix);
		}
	}
	// /Kernel
}

__host__ int darkGray(const int width, const int height, const unsigned char * inputImage, unsigned char * darkGrayImage) {
	// Remember that we now have an interleaved
	cudaError_t devRetVal = cudaSuccess;
	NSTimer globalTimer("GlobalTimer", false, false);
	NSTimer suTimer("SUTimer", false, false);
	NSTimer kernelTimer("KernelTimer", false, false);
	unsigned int N = width*height;
	int blockSize = 4 * THREADS_PER_BLOCK;
	int noBlocks = (N + blockSize - 1)/blockSize;
	int nStreams = 5; 
	int blocksPerStream = noBlocks/nStreams + 1; // plus 1 to adjust for rounding down
	int streamSize = 3 * blockSize * blocksPerStream ; 

	// Device arrays
	unsigned char *devIn, *devOut;

	// Start of the computation
	globalTimer.start();

	// Allocate CUDA memory for input and output
	if ( (devRetVal = cudaMalloc(&devIn, sizeof(unsigned char)*N*3)) != cudaSuccess ) {
		cerr << "Impossible to allocate device memory for image input." << endl;
		return 1;
	}
	if ( (devRetVal = cudaMalloc(&devOut, sizeof(unsigned char)*N)) != cudaSuccess ) {
		cerr << "Impossible to allocate device memory for image output." << endl;
		return 1;
	}
	// Initialize the streams
	cudaStream_t streams[nStreams];
	for (int i = 0; i < nStreams; ++i) {
		cudaStreamCreate(&streams[i]);
	}

	suTimer.start();
	for (int i = 0; i < nStreams; ++i) {
		int offset = i * streamSize; // as the items in the array are one byte large, this is correct
		if( i == (nStreams-1)){
			streamSize = 3 * N - (offset);
		}

		cudaMemcpyAsync(&devIn[offset], &inputImage[offset], streamSize, cudaMemcpyHostToDevice, streams[i]);

		kernelTimer.start();
		darkGrayKernel<<< blocksPerStream, THREADS_PER_BLOCK, 0, streams[i] >>>(N, devIn, devOut, offset);
		kernelTimer.stop();

		// Check if the kernel returned an error
		if ( (devRetVal = cudaGetLastError()) != cudaSuccess ) {
			cerr << "Uh, the kernel had some kind of issue: " << cudaGetErrorString(devRetVal) << endl;
			return 1;
		}

		cudaMemcpyAsync(&darkGrayImage[offset/3], &devOut[offset/3], streamSize/3, cudaMemcpyDeviceToHost, streams[i]);
	}
	cudaDeviceSynchronize();
	suTimer.stop();
	
	// End of the computation
	globalTimer.stop();

	// Print the timers
	// Use the nvprof for actual measurements
	cout << fixed << setprecision(6);
	cout << endl;
	cout << "Total (s): \t" << globalTimer.getElapsed() << endl;
	cout << "Kernel (s): \t" << kernelTimer.getElapsed() << endl;
	cout << "Kernel_and_memory (s): \t" << suTimer.getElapsed() << endl;
	cout << endl;
	cout << setprecision(3);
	// 7 flops in the kernel, 4 times
	cout << "GFLOP/s: \t" << (4 * 7 * N / kernelTimer.getElapsed()) / 1000000000.0 << endl; // Computing power
	// Unsigned char is 8 bits or 1 byte, we read the RGB values (3 unsigned chars) and write the black and white value (1 unsigned chars)
	cout << "GB/s: \t\t" << (4 * ((3*sizeof(unsigned char)*N) + (sizeof(unsigned char))*N) / kernelTimer.getElapsed()) / 1000000000.0 << endl; // Bandwidth, i.e. the amount of data transferred
	cout << endl;

	cudaFree(devIn);
	cudaFree(devOut);

	return 0;
}