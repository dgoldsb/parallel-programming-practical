#include <Timer.hpp>
#include <iostream>
#include <iomanip>

using LOFAR::NSTimer;
using std::cout;
using std::cerr;
using std::endl;
using std::fixed;
using std::setprecision;


// row per stream for smooth
__global__ void triangularSmoothKernel(const int width, const int height, const int spectrum, unsigned char * inputImage, unsigned char * smoothImage, const unsigned int offset, const int streamID) {
	float filter[] = {1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 2.0f, 2.0f, 2.0f, 1.0f, 1.0f, 2.0f, 3.0f, 2.0f, 1.0f, 1.0f, 2.0f, 2.0f, 2.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f};
	__shared__ unsigned char inputPix[32][32][3];

	// What is the first pixel (leftup-most pixel) part of the apron that is included?
	int leftUpX = blockIdx.x*24-4;
	int leftUpY = (streamID*10 + blockIdx.y)*24-4;
	
	// What is the pixel in the image associated to this thread?
	int threadX = (leftUpX+threadIdx.x);
	int threadY = (leftUpY+threadIdx.y);

	unsigned int RGBitem = threadX * 3 + threadY * width * 3;

	// Filter out all threads that go beyond the height or width of the image
	if((threadX < width)&&(threadY < height)&&(threadX > -1)&&(threadY > -1)){

		// Fill the shared array, including apron
		for ( int z = 0; z < spectrum; z++ ) {
			inputPix[threadIdx.x][threadIdx.y][z] = inputImage[RGBitem+z];
		}
		__syncthreads();

		// Filter out the threads that are associated to the apron
		if((threadIdx.x > 3)&&(threadIdx.x < 28)&&(threadIdx.y > 3)&&(threadIdx.y < 28)){
			int x = threadX;
			int y = threadY;
			for ( int z = 0; z < spectrum; z++ ) {
					unsigned int filterItem = 0;
					float filterSum = 0.0f;
					float smoothPix = 0.0f;

					for ( int fy = y - 2; fy < y + 3; fy++ ) {
						if ( fy < 0 ) {
							filterItem += 5;
							continue;
						}
						else if ( fy == height ) {
							break;
						}
						
						for ( int fx = x - 2; fx < x + 3; fx++ ) {
							if ( (fx < 0) || (fx >= width) ) {
								filterItem++;
								continue;
							}

							// Reading from the shared array is tricky, since we need to use the offset and threadIdx
							int fxshared = threadIdx.x + (fx - x);
							int fyshared = threadIdx.y + (fy - y);
							smoothPix += static_cast< float >(inputPix[fxshared][fyshared][z]) * filter[filterItem];
							filterSum += filter[filterItem];
							filterItem++;
						}
					}
				smoothPix /= filterSum;
				smoothImage[RGBitem + z] = static_cast< unsigned char >(smoothPix + 0.5f);
			}
		}
	}
	// /Kernel
}


int triangularSmooth(const int width, const int height, const int spectrum, unsigned char * inputImage, unsigned char * smoothImage) {
	cudaError_t devRetVal = cudaSuccess;
	NSTimer globalTimer("GlobalTimer", false, false);
	NSTimer suTimer("SUTimer", false, false);
	NSTimer kernelTimer("KernelTimer", false, false);
	unsigned char *devIn, *devOut;
	
	// Determine the number of blocks I need
	// Remember, the apron has a width of 4 all around!
	dim3 threadsPerBlock(32, 32);
	dim3 numBlocks(width/(threadsPerBlock.x-8)+1,height/(threadsPerBlock.y-8)+1); 

	// Prepare for streams
	unsigned int N = width * height;
	// int blockSize = threadsPerBlock.x * threadsPerBlock.y;
	// int noBlocks = numBlocks.x * numBlocks.y;
	dim3 blocksPerStream(numBlocks.x, 10);
	int nStreams = numBlocks.y / 10 + 1; // plus 1 to adjust for rounding down
	
	// Start of the computation
	globalTimer.start();

	// Allocate CUDA memory for input
	if ( (devRetVal = cudaMalloc(&devIn, sizeof(unsigned char)*N*spectrum)) != cudaSuccess ) {
		cerr << "Impossible to allocate device memory for image input." << endl;
		return 1;
	}
	// Allocate CUDA memory for output
	if ( (devRetVal = cudaMalloc(&devOut, sizeof(unsigned char)*N*spectrum)) != cudaSuccess ) {
		cerr << "Impossible to allocate device memory for image output." << endl;
		return 1;
	}

	// Initialize the streams
	cudaStream_t streams[nStreams];
	for (int i = 0; i < nStreams; ++i) {
		cudaStreamCreate(&streams[i]);
	}
	
	int inStreamSize = spectrum * (threadsPerBlock.y-4) * width * 10; 
	int outStreamSize = 24*width*spectrum*10;

	suTimer.start();
	for (int i = 0; i < nStreams; ++i) {
		unsigned int offset = i * spectrum * (threadsPerBlock.y-8) * width * 10; // first pixel to actually do
		if((279+(offset/width)/3) > height){
			inStreamSize = spectrum * N - offset;
			outStreamSize = spectrum * N - offset;
		}
		if((239+(offset/width)/3) > height){
			inStreamSize = spectrum * N - offset;
			outStreamSize = spectrum * N - offset;
		}
		
		cudaMemcpyAsync(&devIn[offset], &inputImage[offset], inStreamSize, cudaMemcpyHostToDevice, streams[i]);

		kernelTimer.start();
		triangularSmoothKernel<<< blocksPerStream, threadsPerBlock, 0, streams[i] >>>(width, height, spectrum, devIn, devOut, offset, i);
		kernelTimer.stop();
		// Check if the kernel returned an error
		if ( (devRetVal = cudaGetLastError()) != cudaSuccess ) {
			cerr << "Uh, the kernel had some kind of issue: " << cudaGetErrorString(devRetVal) << endl;
			return 1;
		}

		cudaMemcpyAsync(&smoothImage[offset], &devOut[offset], outStreamSize, cudaMemcpyDeviceToHost, streams[i]);
		if((239+(offset/width)/3) > height){
			nStreams = i+1;
			break;
		}
	}

	cudaDeviceSynchronize();
	suTimer.stop();

	// End of the computation
	globalTimer.stop();

	// Print the timers
	// Use the nvprof for actual measurements
	cout << fixed << setprecision(6);
	cout << endl;
	cout << "Total (s): \t" << globalTimer.getElapsed() << endl;
	cout << "Kernel (s): \t" << kernelTimer.getElapsed() << endl;
	cout << "Kernel_and_memory (s): \t" << suTimer.getElapsed() << endl;
	cout << endl;
	cout << setprecision(3);
	// 3 flops per loop in the kernel, loops 25 times, so 75 flops for every pixel
	cout << "GFLOP/s: \t" << (75 * N/ 1000000000.0)  / kernelTimer.getElapsed() << endl; // Computing power
	// Unsigned char is 8 bits or 1 byte
	// Every thread accesses from global memory 3 times for a color image, 1 time for a BW
	// In this calculation we assume color
	// All threads do this, also those assigned to the apron, so (32*32*)
	// Storing is the same amount of traffic, and is done by N threads
	cout << N << endl;
	unsigned int noReadingThreads = numBlocks.x * numBlocks.y * threadsPerBlock.x * threadsPerBlock.y;
	cout << noReadingThreads << endl;
	cout << "GB/s: \t\t" << ((((noReadingThreads/ 1000000000.0) * 3 * sizeof(unsigned char)) + ((N/ 1000000000.0)) * 3 * sizeof(unsigned char))) / kernelTimer.getElapsed() << endl; // Bandwidth, i.e. the amount of data transferred
	cout << endl;

	cudaFree(devIn);
	cudaFree(devOut);

	return 0;
}
