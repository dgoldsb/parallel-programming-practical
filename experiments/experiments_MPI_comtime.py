# Written by Geert Konijnendijk 
# Adapted by Dylan Goldsborough (with permission)
# 4 spaces per indentation level

from subprocess import *
import re
import shlex
import sys

COMMANDSEQ = "prun -v -np 1 -1 -sge-script $PRUN_ETC/prun-openmpi ../mpi1516/nbody/nbody-seq %d 0 nbody.ppm %d"
COMMANDPAR = "prun -v -np %d -1 -sge-script $PRUN_ETC/prun-openmpi ../mpi1516/nbody/nbody-par %d 0 nbody.ppm %d"
COMMANDPARO = "prun -v -np %d -1 -sge-script $PRUN_ETC/prun-openmpi ../mpi1516/nbody/nbody-par-beforeimpr %d 0 nbody.ppm %d"
COMMANDCOM = "prun -v -np %d -1 -sge-script $PRUN_ETC/prun-openmpi ../mpi1516/nbody/nbody-parcom %d 0 nbody.ppm %d"
TIME_REGEX = re.compile(r'N\-body\stook\s+(\d*\.\d*)\sseconds')

bodies = [128, 256, 512]
iterations = [10000]
nodes = [2,4,8,16]
average = 10

f = open(sys.argv[1], "w")
err = open(sys.argv[2], "w")

f.write("Bodies Timesteps Nodes Iteration Par_Time Com_Time\n\n")

for b in bodies:
    for i in iterations:     
        for n in nodes:
            print("%d bodies %d timesteps %d nodes" % (b,i,n))
            for a in range(average):
                commandpar = shlex.split(COMMANDPAR % (n, b, i))
                commandparcom = shlex.split(COMMANDCOM % (n, b, i))
                par_time = 0.0
                com_time = 0.0
                
                p = Popen(commandpar, stderr=PIPE, stdout=PIPE)
                _, prunerr = p.communicate()
                err.write("#####################\n")
                err.write("%d %d %d %d\n" % (b, i, n, a))
                err.write("#####################\n")
                err.write(prunerr)

                node_times = []
                for l in prunerr.split("\n"):
                    m = TIME_REGEX.match(l)
                    if m:
                        group = m.group(1)
                        if group:
                            node_times.append(float(group))
                par_time = max(node_times)

                p = Popen(commandparcom, stderr=PIPE, stdout=PIPE)
                _, prunerr = p.communicate()
                err.write(prunerr)

                node_times = []
                for l in prunerr.split("\n"):
                    m = TIME_REGEX.match(l)
                    if m:
                        group = m.group(1)
                        if group:
                            node_times.append(float(group))
                com_time = max(node_times)  
				
                f.write("%d %d %d %d %f %f \n" % (b, i, n, a, par_time, com_time))
                f.flush()

f.close()
# out.close()
err.close()