# Written by Geert Konijnendijk 
# Adapted by Dylan Goldsborough (with permission)
# 4 spaces per indentation level

from subprocess import *
import re
import shlex
import sys

COMMANDSEQ = "prun -v -np 1 -1 -sge-script $PRUN_ETC/prun-openmpi ../mpi1516/nbody/nbody-seq %d 0 nbody.ppm %d"
COMMANDPAR = "prun -v -np %d -1 -sge-script $PRUN_ETC/prun-openmpi ../mpi1516/nbody/nbody-par %d 0 nbody.ppm %d"
COMMANDPARO = "prun -v -np %d -1 -sge-script $PRUN_ETC/prun-openmpi ../mpi1516/nbody/nbody-par-beforeimpr %d 0 nbody.ppm %d"
COMMANDCOM = "prun -v -np %d -1 -sge-script $PRUN_ETC/prun-openmpi ../mpi1516/nbody/nbody-parcom %d 0 nbody.ppm %d"
TIME_REGEX = re.compile(r'N\-body\stook\s+(\d*\.\d*)\sseconds')

bodies = [64, 128, 256]
iterations = [1000, 10000, 100000]
nodes = [2,4,8,16]
average = 10

f = open(sys.argv[1], "w")
err = open(sys.argv[2], "w")

f.write("Bodies Timesteps Nodes Iteration Seq_Time Par_Time Par_Time_Unopt Com_Time Speedup\n\n")

for b in bodies:
    for i in iterations:
        seq_times = []
        for sa in range(average):
            command = shlex.split(COMMANDSEQ % (b, i))
            p = Popen(command, stderr=PIPE, stdout=PIPE)
            _, prunerr = p.communicate()
    
            err.write("#####################\n")
            err.write("%d %d 1 %d\n" % (b, i, sa))
            err.write("#####################\n")
            err.write(prunerr)
            node_times = []
            for l in prunerr.split("\n"):
                m = TIME_REGEX.match(l)
                if m:
                    group = m.group(1)
                    if group:
                        node_times.append(float(group))
            seq_times.append(max(node_times))
        seq_time = sum(seq_times) / float(len(seq_times))
        
        for n in nodes:
            print("%d bodies %d timesteps %d nodes" % (b,i,n))
            for a in range(average):
                commandpar = shlex.split(COMMANDPAR % (n, b, i))
                commandparo = shlex.split(COMMANDPARO % (n, b, i))
                commandparcom = shlex.split(COMMANDCOM % (n, b, i))
                par_time = 0.0
                paro_time = 0.0
                com_time = 0.0
                su = 0.0
                
                p = Popen(commandpar, stderr=PIPE, stdout=PIPE)
                _, prunerr = p.communicate()
                err.write("#####################\n")
                err.write("%d %d %d %d\n" % (b, i, n, a))
                err.write("#####################\n")
                err.write(prunerr)

                node_times = []
                for l in prunerr.split("\n"):
                    m = TIME_REGEX.match(l)
                    if m:
                        group = m.group(1)
                        if group:
                            node_times.append(float(group))
                par_time = max(node_times)

                p = Popen(commandparo, stderr=PIPE, stdout=PIPE)
                _, prunerr = p.communicate()
                err.write(prunerr)

                node_times = []
                for l in prunerr.split("\n"):
                    m = TIME_REGEX.match(l)
                    if m:
                        group = m.group(1)
                        if group:
                            node_times.append(float(group))
                paro_time = max(node_times)

                p = Popen(commandparcom, stderr=PIPE, stdout=PIPE)
                _, prunerr = p.communicate()
                err.write(prunerr)

                node_times = []
                for l in prunerr.split("\n"):
                    m = TIME_REGEX.match(l)
                    if m:
                        group = m.group(1)
                        if group:
                            node_times.append(float(group))
                com_time = max(node_times)                
                            
                su = seq_time/par_time        
                f.write("%d %d %d %d %f %f %f %f %f \n" % (b, i, n, a, seq_time, par_time, paro_time, com_time, su))
                f.flush()
f.close()
# out.close()
err.close()