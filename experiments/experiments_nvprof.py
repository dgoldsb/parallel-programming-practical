# Written by Geert Konijnendijk 
# Adapted by Dylan Goldsborough (with permission)
# 4 spaces per indentation level

from subprocess import *
import re
import shlex
import sys

COMMANDTIME = "prun -v -np 1 -native '-l gpu=GTX480' nvprof ../GPU-15-16/bin/%sCUDA ../images/%s"
COMMANDMETRICS = "prun -v -np 1 -native '-l gpu=GTX480' nvprof --metrics flops_sp --metrics dram_read_throughput ../GPU-15-16/bin/%sCUDA ../images/%s"
TIME_REGEX_TIME = re.compile(r'\s+\d*.\d*%\s+\d*.\d*..\s+\d*\s+(\d*.\d*)(..)\s+\d*.\d*..\s+\d*.\d*..\s+\w*Kernel')
TIME_REGEX_METRICS = re.compile(r'\s+\d*\s+flops_sp\s+FLOPS\(Single\)\s+\d*\s+\d*\s+(\d*)|\s+\d*\s+dram_read_throughput\s+Device Memory Read Throughput\s+\d*\.\d*..\/s\s+\d*\.\d*..\/s\s+(\d*\.\d*)(..)\/s')

images = ['image00.png','image01.jpg','image02.jpg','image03.jpg','image04.jpg','image05.jpg','image06.jpg']
imgfilter = ['darker','histogram','smooth']
#

f = open(sys.argv[1], "w")
err = open(sys.argv[2], "w")

f.write("# Filter Image GFLOP Bandwidth\n")

for fil in imgfilter:
    for img in images:
        print("%s %s" % (fil,img))
        commandtime = shlex.split(COMMANDTIME % (fil,img))
        commandmet = shlex.split(COMMANDMETRICS % (fil,img))
        bw = gflop = -1

        p = Popen(commandtime, stderr=PIPE, stdout=PIPE)
        prunout, prunerr = p.communicate()
        err.write(prunerr)
        err.write(prunout)

        timing = -1.0
        timeformat = 's'
        for l in prunerr.split("\n"):
            m = TIME_REGEX_TIME.match(l)
            if m:
                group = m.group(1)
                if group:
                    timing = float(group)
                group = m.group(2)
                if group:
                    timeformat = str(group)

        p = Popen(commandmet, stderr=PIPE, stdout=PIPE)
        prunout, prunerr = p.communicate()
        err.write(prunerr)
        err.write(prunout)

        flops = -1
        bwformat = 'B'
        for l in prunerr.split("\n"):
            m = TIME_REGEX_METRICS.match(l)
            if m:
                group = m.group(1)
                if group:
                    flops = int(group)
                group = m.group(2)
                if group:
                    bw = float(group)
                group = m.group(3)
                if group:
                    bwformat = str(group)

        if(bwformat=='MB'):
            bw = bw/1000
        if(timeformat=='us'):
            gflop = flops/1000/timing
        elif(timeformat=='ms'):
            gflop = flops/1000000/timing
        elif(timeformat=='s'):
            gflop = flops/1000000000/timing
        

        f.write("%s %s %f %f\n" % (fil, img, gflop, bw))
        f.flush()
f.close()
# out.close()
err.close()