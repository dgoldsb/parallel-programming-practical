# Written by Geert Konijnendijk 
# Adapted by Dylan Goldsborough (with permission)
# 4 spaces per indentation level

from subprocess import *
import re
import shlex
import sys

COMMANDSEQ = "prun -v -1 -np 1 java -cp \".:../ppp1516_java/lib/*\" rubiks.sequential.Rubiks --size %d --seed %d"
COMMANDPAR = "prun -v -1 -np %d java -cp \".:../ppp1516_java/lib/*\" -Dibis.pool.size=%d -Dibis.pool.name=rubiks -Dibis.server.address=130.37.199.7-9294 rubiks.ipl.Rubiks --size %d --seed %d"
TIME_REGEX_TIME = re.compile(r'Solving cube took (\d*) milliseconds')

sizes = [4]
seeds = [16] # 14, 16, 18
nodes = [1,2,4,8,16]
#

f = open(sys.argv[1], "w")
err = open(sys.argv[2], "w")

f.write("# Nodes Seed Size Sequential Parallel Speedup\n")

for size in sizes:
    for seed in seeds:
        seq = -1
        commandseq = shlex.split(COMMANDSEQ % (size,seed))
        p = Popen(commandseq, stderr=PIPE, stdout=PIPE)
        prunout, prunerr = p.communicate()
        err.write(prunerr)
        err.write(prunout)

        for l in prunerr.split("\n"):
            m = TIME_REGEX_TIME.match(l)
            if m:
                group = m.group(1)
                if group:
                    seq = int(group)

        for node in nodes:
            print("%s %s %s" % (seed,size,node))
            commandpar = shlex.split(COMMANDPAR % (node,node,size,seed))
            par = su = -1

            
            p = Popen(commandpar, stderr=PIPE, stdout=PIPE)
            prunout, prunerr = p.communicate()
            err.write(prunerr)
            err.write(prunout)

            for l in prunerr.split("\n"):
                m = TIME_REGEX_TIME.match(l)
                if m:
                    group = m.group(1)
                    if group:
                        par = int(group)
            su = float(seq)/float(par)
            
            f.write("%d %d %d %d %d %f\n" % (node, seed, size, seq, par, su))
            f.flush()
f.close()
err.close()