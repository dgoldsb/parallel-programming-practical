from subprocess import *
import re
import shlex
import sys

COMMAND = "prun -v -1 -np %d -sge-script $PRUN_ETC/prun-openmpi nbody/nbody-par %d 0 nbody.ppm %d"
TIME_REGEX = re.compile(r'N\-body\stook\s+(\d*\.\d*)\sseconds')

bodies = [64, 128, 256, 512]
iterations = [1000, 10000, 100000]
nodes = [2,4,8,16]
average = 25

f = open(sys.argv[1], "w")
err = open(sys.argv[2], "w")

f.write("bodies iterations nodes iteration time\n")

for b in bodies:
    for i in iterations:
        for n in nodes:
            for a in range(average):
                command = shlex.split(COMMAND % (n, b, i))
                p = Popen(command, stderr=PIPE, stdout=PIPE)
                _, prunerr = p.communicate()
		
                err.write("#####################\n")
                err.write("%d %d %d %d\n" % (b, i, n, a))
                err.write("#####################\n")
                err.write(prunerr)

                node_times = []
                for l in prunerr.split("\n"):
                    m = TIME_REGEX.match(l)
                    if m:
                        group = m.group(1)
                        if group:
                            node_times.append(float(group))
                f.write("%d %d %d %d %f\n" % (b, i, n, a, max(node_times)))
                f.flush()
f.close()
# out.close()
err.close()
