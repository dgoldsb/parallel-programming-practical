# Written by Geert Konijnendijk 
# Adapted by Dylan Goldsborough (with permission)
# 4 spaces per indentation level

from subprocess import *
import re
import shlex
import sys

COMMANDSEQ = "prun -v -np 1 -native '-l gpu=GTX480' ../GPU-15-16/bin/%sSeq ../images/%s"
COMMANDPAR = "prun -v -np 1 -native '-l gpu=GTX480' ../GPU-15-16/bin/%sCUDA ../images/%s"
TIME_REGEX_S = re.compile(r'(\d*\.\d*)')
TIME_REGEX_P = re.compile(r'\w*\s\(\w\):\s+(\d*\.\d*)|\w*\/\w:\s+(\d*\.\d*)')

images = ['image00.png','image01.jpg','image02.jpg','image03.jpg','image04.jpg','image05.jpg','image06.jpg']
sizes = [262144, 2621440, 3686400, 25240576, 16777216, 33141100, 65536000, -1]
imgfilter = ['darker','histogram','smooth']
average = 20
#

f = open(sys.argv[1], "w")
err = open(sys.argv[2], "w")

f.write("# Filter Image Bytes Iteration Seq_Kernel Par_Total Par_Kernel Par_GFLOP Par_Bandwidth Speedup\n")

for fil in imgfilter:
    imgid = -1
    for img in images:
        imgid = imgid + 1
        print("%s %s" % (fil,img))
        for a in range(average):
            commandpar = shlex.split(COMMANDPAR % (fil,img))
            commandseq = shlex.split(COMMANDSEQ % (fil,img))
            sk = pt = pkm = pk = pgflop = pbw = su = -1
            
            p = Popen(commandseq, stderr=PIPE, stdout=PIPE)
            prunout, prunerr = p.communicate()
            err.write("#####################\n")
            err.write("%s %s %d\n" % (fil,img,a))
            err.write("#####################\n")
            err.write(prunerr)
            err.write(prunout)

            times = []
            for l in prunout.split("\n"):
                m = TIME_REGEX_S.match(l)
                if m:
                    group = m.group(1)
                    if group:
                        times.append(float(group))
            if (0 < len(times)):
                try:
                    sk = times[0]
                except IndexError:
                    print("Well that failed.")

            p = Popen(commandpar, stderr=PIPE, stdout=PIPE)
            prunout, prunerr = p.communicate()
            err.write(prunerr)
            err.write(prunout)

            times = []
            for l in prunout.split("\n"):
                m = TIME_REGEX_P.match(l)
                if m:
                    group = m.group(1)
                    if group:
                        times.append(float(group))
                    group = m.group(2)
                    if group:
                        times.append(float(group))
            if (len(times) > 0):
                try:
                    pt = times[0]
                except IndexError:
                    print("Well that failed.")
                try:
                    pkm = times[2]
                except IndexError:
                    print("Well that failed.")
                try:
                    pk = times[1]
                except IndexError:
                    print("Well that failed.")
                try:
                    pgflop = times[3]
                except IndexError:
                    print("Well that failed.") 
                try:
                    pbw = times[4]
                except IndexError:
                    print("Well that failed.")
                su = sk / (pkm)
            f.write("%s %s %f %d %f %f %f %f %f %f \n" % (fil, img, sizes[imgid], a, sk, pkm, pk, pgflop, pbw, su))
            f.flush()
f.close()
# out.close()
err.close()